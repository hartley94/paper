# <img src="./data/icons/hicolor/scalable/apps/io.posidon.Paper.svg" height="64"/>Paper

## Take notes in Markdown

Contributions are appreciated!


## Some of Paper features:

 - Almost WYSIWYG markdown rendering

 - Searchable through GNOME search

 - Highlight and strikethrough text formatting

 - Application theming based on notebook color

 - Trash can

 - Markdown document

## Get Paper

The recommended way of installing Paper is through [Flatpak](https://flatpak.org)

<a href="https://flathub.org/apps/details/io.posidon.Paper"><img src="https://flathub.org/assets/badges/flathub-badge-en.png" width="200"/></a>

## Libraries Used
 - [libadwaita](https://gitlab.gnome.org/GNOME/libadwaita)
 - [gtksourceview-5](https://gitlab.gnome.org/GNOME/gtksourceview)

## License
The source code is GPLv3

## Notes Storage
By default, notes are stored in `~/.var/app/io.posidon.Paper/data`,
but that can be changed in preferences

## Build Instructions
Flatpak build requires flatpak-building installed.

### Local builds (NOT RECOMMENDED)
 - change into the top level source directory
 - to configure the build environment (required only once), run ```meson build```
 - change into the build directory
 - to build Paper, run ```ninja```
 - to install Paper, run ```ninja install```

### Flatpak builds
 - change into the top level source directory
 - to build the flatpak, run ```flatpak-builder flatpak io.posidon.Paper.json```
 - to install the flatpak, run ```flatpak-builder --user --install --force-clean flatpak io.posidon.Paper.json```
 - to launch the flatpak, run ```flatpak run io.posidon.Paper```

## Release instructions
 Paper uses a YY.## version format string, where YY is the two digit year (aka 23, 24, 25, etc) and ## is the release number of the year (aka 01 for the first release, 02 for the second release, etc., not the month number).

 The release version is located in the main `meson.build` file, no other files contain the version number.

 The changelog is located in `data/app.metainfo.xml.in`.
